/*
Helper functions for Bubbles
*/

// fractional part
float fract(float x) { return x - floor(x); }

float hashNull(float2 x)
{
    float r = fract(523.0 * sin(dot(x, float2(53.3158, 43.6143))));
    return r;
}

float4 hash4(float4 n) 
{
    float v0 = fract(sin(n.x) * 753.5453123);
    float v1 = fract(sin(n.y) * 753.5453123);
    float v2 = fract(sin(n.z) * 753.5453123);
    float v3 = fract(sin(n.w) * 753.5453123);
    return float4(v0, v1, v2, v3);
}
float2 hash2(float2 n)
{
    float v0 = fract(sin(n.x) * 753.5453123);
    float v1 = fract(sin(n.y) * 753.5453123);
	return float2(v0, v1); 
}

float2 hash2a(float2 x, float anim)
{
    float r = 523.0 * sin(dot(x, float2(53.3158, 43.6143)));
    float xa1 = fract(anim);
    float xb1 = anim - xa1;
    anim += 0.5;
    float xa2 = fract(anim);
    float xb2 = anim - xa2;
    
    float2 z1 = float2(fract(15.32354 * (r + xb1)), fract(17.25865 * (r + xb1)));
    r = r + 1.0;
    float2 z2 = float2(fract(15.32354 * (r + xb1)), fract(17.25865 * (r + xb1)));
    r = r + 1.0;
    float2 z3 = float2(fract(15.32354 * (r + xb2)), fract(17.25865 * (r + xb2)));
    r = r + 1.0;
    float2 z4 = float2(fract(15.32354 * (r + xb2)), fract(17.25865 * (r + xb2)));
    return (lerp(z1, z2, xa1) + lerp(z3, z4, xa2)) * 0.5;
}

// color noise lerped 1d
float noise2(float2 x)
{
    float2 p = floor(x);
    float2 f = float2(fract(x.x), fract(x.y));
    f = f * f * (3.0 - 2.0 * f);

    float4 NC0 = float4(0.0, 157.0, 113.0, 270.0);
    float4 NC1 = float4(1.0, 158.0, 114.0, 271.0);
	
    float n = p.x + p.y * 157.0;
    float2 s1 = lerp(hash2(float2(n,n) + NC0.xy), hash2(float2(n,n) + NC1.xy), float2(f.x,f.x));
    return lerp(s1.x, s1.y, f.y);
}

// color noise lerped 2d
float noise3(float3 x)
{
    float3 p = float3(floor(x.x), floor(x.y), floor(x.z));
    float3 f = float3(fract(x.x), fract(x.y), fract(x.z));
    
	float3 oldF = f;
    f = f * f;
    float3 t = float3(3.0, 3.0, 3.0) - (2.0 * oldF);
    f = f * t;
	
	float n = p.x + dot(p.yz, float2(157.0, 113.0));
	
    float4 NC0 = float4(0.0, 157.0, 113.0, 270.0);
    float4 NC1 = float4(1.0, 158.0, 114.0, 271.0);

    float4 a = hash4(float4(n, n, n, n) + NC0);
    float4 b = hash4(float4(n, n, n, n) + NC1);

    float4 s1 = lerp(a, b, float4(f.x,f.x,f.x,f.x));

    return lerp(lerp(s1.x, s1.y, f.y), lerp(s1.z, s1.w, f.y), f.z);
}

float4 booble(float2 te, float2 pos, float numCells)
{
    float d = dot(te, te);
    //if (d>=0.06) return float4(0.0);

    float2 te1 = te + (pos - float2(0.5, 0.5)) * 0.4 / numCells;
    float2 te2 = -te1;
    float zb1 = max(pow(noise2(te2 * 1000.11 * d), 10.0), 0.01);
    float zb2 = noise2(te1 * 1000.11 * d);
    float zb3 = noise2(te1 * 200.11 * d);
    float zb4 = noise2(te1 * 200.11 * d + float2(20.0, 20.0));
    
    float4 colorb = float4(1.0, 1.0, 1.0, 1.0);
    colorb.xyz = colorb.xyz * (0.7 + noise2(te1 * 1000.11 * d) * 0.3);
    
    zb2 = max(pow(zb2, 20.1), 0.01);
    colorb.xyz = colorb.xyz * (zb2 * 1.9);
    
    float4 color = float4(noise2(te2 * 10.8), noise2(te2 * 9.5 + float2(15.0, 15.0)), noise2(te2 * 11.2 + float2(12.0, 12.0)), 1.0);
    color = lerp(color, float4(1.0, 1.0, 1.0, 1.0), noise2(te2 * 20.5 + float2(200.0, 200.0)));
    color.xyz = color.xyz * (0.7 + noise2(te2 * 1000.11 * d) * 0.3);
    color.xyz = color.xyz * (0.2 + zb1 * 1.9);
    
    float r1 = max(min((0.033 - min(0.04, d)) * 100.0 / sqrt(numCells), 1.0), -1.6);
    float d2 = (0.06 - min(0.06, d)) * 10.0;
    d = (0.04 - min(0.04, d)) * 10.0;
    color.xyz = color.xyz + colorb.xyz * d * 1.5;
    
    float f1 = min(d * 10.0, 0.5 - d) * 2.2;
    f1 = pow(f1, 4.0);
    float f2 = min(min(d * 4.1, 0.9 - d) * 2.0 * r1, 1.0);

    float f3 = min(d2 * 2.0, 0.7 - d2) * 2.2;
    f3 = pow(f3, 4.0);
    
    float v = color * max(min(f1 + f2, 1.0), -0.5) + float4(zb3, zb3, zb3, zb3) * f3 - float4(zb4, zb4, zb4, zb4) * (f2 * 0.5 + f1) * 0.5;
    return float4(v, v, v, v);
}

float4 Cells(float2 p, float2 move, in float numCells, in float count, float blur, float iTime)
{
	float2 uv = p;
    float2 inp = p + move;
    inp *= numCells;
    float d = 1.0;
    float2 te;
    float2 pos;
    for (int xo = -1; xo <= 1; xo++)
    {
        for (int yo = -1; yo <= 1; yo++)
        {
            float2 tp = floor(inp) + float2(xo, yo);
            float2 rr = fmod(tp, numCells);
            tp = tp + (hash2a(rr, iTime * 0.1) + hash2a(rr, iTime * 0.1 + 0.25)) * 0.5;
            float2 l = inp - tp;
            float dr = dot(l, l);
            if (hashNull(rr) > count)
                if (d > dr)
                {
                    d = dr;
                    pos = tp;
                }
        }
    }
    if (d >= 0.06)
        return float4(0.0, 0.0, 0.0, 0.0);
    te = inp - pos;
    
    //te=te+(te*noise3(vec3(te*5.9,iTime*40.0))*0.02);
    //te=te+(te*(noise3(vec3(te*3.9+p,iTime*0.2))+noise3(vec3(te*3.9+p,iTime*0.2+0.25))+noise3(vec3(te*3.9+p,iTime*0.2+0.5))+noise3(vec3(te*3.9+p,iTime*0.2+0.75)))*0.05);
     
    if (d < 0.04)
        uv = uv + te * (d) * 2.0;
    if (blur > 0.0001)
    {
        float4 c = float4(0.0, 0.0, 0.0, 0.0);
        for (float x = -1.0; x < 1.0; x += 0.5)
        {
            for (float y = -1.0; y < 1.0; y += 0.5)
            {
                c += booble(te + float2(x, y) * blur, p, numCells);
            }
        }
        return c * 0.05;
    }

    return booble(te, p, numCells);
}