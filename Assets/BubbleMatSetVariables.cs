﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class BubbleMatSetVariables : MonoBehaviour {

	[SerializeField] private Material _mat;
	private Material mat
	{
		get
		{
			if (_mat == null)
			{
				_mat = GetComponent<MeshRenderer>().material;
			}
			return _mat;
		}
	}

	private float time = 0;

	private void Start()
	{
		time = 0;
	}

	// Update is called once per frame
	void Update ()
	{
		if (!mat)
			return;

		mat.SetFloat("_iTime", time);
		time += Time.deltaTime;

		if (time > 99999)
			time = 0;
	}
}
