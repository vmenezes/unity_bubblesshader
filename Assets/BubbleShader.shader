﻿/*
Shader created by Vitor for game analyzis.

It creates bubbles without any textures and using only fragment shader.

Reference: https://www.shadertoy.com/view/llsSDf
*/

Shader "Bubbles/BubbleShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_iTime ("Time", Float) = 0.0
	}
	SubShader
	{
		Tags {"Queue" = "Transparent" "RenderType" = "Transparent" }
		LOD 100

		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"
			#include "BubblesShader.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float _iTime;
			
			/*
			Just to pass the uv
			*/
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				return o;
			}
			
			/*
			Check BubblesShader.cginc for more details.
			*/
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv);
			
				float2 uv = i.uv;
				
				float2 l1 = float2(_iTime*0.02, _iTime*0.02);
				float2 l2 = float2(-_iTime*0.01, _iTime*0.007);
				float2 l3 = float2(0.0, _iTime*0.01);
				
				// calculates background
				float4 image = float4(noise3(float3(uv*2.0, _iTime*0.1)),
									  noise3(float3(uv*2.0 + float2(200.0, 200.0), _iTime*0.1)),
									  noise3(float3(uv*2.0 + float2(50.0, 50.0), _iTime*0.1)),
									  0.5);

				// Create Bubbles (Simple cell algorithm)
				float4 cr1 = Cells(uv, float2(20.2449, 93.78) + l1	, 1.5	, 0.5	, 0.0	, _iTime);
				float4 cr2 = Cells(uv, float2(15.0, 400.0)			, 3.0	, 0.5	, 0.003	, _iTime);
				float4 cr3 = Cells(uv, float2(230.79, 193.2) + l2	, 4.0	, 0.5	, 0.0	, _iTime);
				float4 cr4 = Cells(uv, float2(200.19, 393.2) + l3	, 7.0	, 0.8	, 0.025	, _iTime);
				float4 cr5 = Cells(uv, float2(10.3245, 233.645) + l3, 9.2	, 0.9	, 0.025	, _iTime);
				float4 cr6 = Cells(uv, float2(10.3245, 233.645) + l3, 14.2	, 0.95	, 0.05	, _iTime);
				float4 cr7 = Cells(uv, float2(10.3245, 233.645) + l3, 20.2	, 0.95	, 0.05	, _iTime);

				
				// Calculates when bubble and when background - since bubbles are white, max suffices
				float d = dot(cr7, cr7);
				image = max(image - float4(d, d, d, d)*0.1, 0.0) + cr7 * 1.6;

				d = dot(cr6, cr6);
				image = max(image - float4(d, d, d, d)*0.1, 0.0) + cr6 * 1.6;

				d = dot(cr5, cr5);
				image = max(image - float4(d, d, d, d)*0.1, 0.0) + cr5 * 1.6;

				d = dot(cr4, cr4);
				image = max(image - float4(d, d, d, d)*0.1, 0.0) + cr4 * 1.3;
				
				d = dot(cr3, cr3);
				image = max(image - float4(d, d, d, d)*0.1, 0.0) + cr3 * 1.1;

				d = dot(cr2, cr2);
				image = max(image - float4(d, d, d, d)*0.1, 0.0) + cr2 * 1.4;

				d = dot(cr1, cr1);
				image = max(image - float4(d, d, d, d)*0.1, 0.0) + cr1 * 1.8;
				
				return image;
			}
			ENDCG
		}
	}
}
